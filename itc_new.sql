-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 08:43 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itc`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(4),
(4),
(4),
(4),
(4),
(4);

-- --------------------------------------------------------

--
-- Table structure for table `t_category`
--

CREATE TABLE `t_category` (
  `id_category` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_comment`
--

CREATE TABLE `t_comment` (
  `id_comment` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `date_created` varchar(255) DEFAULT NULL,
  `status_verification` bit(1) DEFAULT NULL,
  `id_notification` int(11) DEFAULT NULL,
  `id_topic` int(11) DEFAULT NULL,
  `id_topic_acc` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_notification`
--

CREATE TABLE `t_notification` (
  `id_notification` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `date_received` varchar(255) DEFAULT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_topic`
--

CREATE TABLE `t_topic` (
  `id_topic` int(11) NOT NULL,
  `date_created` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `total_agree` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_topic`
--

INSERT INTO `t_topic` (`id_topic`, `date_created`, `description`, `picture`, `status`, `title`, `total_agree`, `id_user`) VALUES
(1, NULL, 'Ini vote dulu', NULL, b'1111111111111111111111111111111', 'Lalala', 0, NULL),
(2, NULL, 'fanfna', NULL, b'1111111111111111111111111111111', 'Sama', 0, NULL),
(3, NULL, 'fqgnqg', NULL, b'1111111111111111111111111111111', 'bffa', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_topic_accepted`
--

CREATE TABLE `t_topic_accepted` (
  `id_topic_acc` int(11) NOT NULL,
  `date_created` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `total_agree` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_topic_category`
--

CREATE TABLE `t_topic_category` (
  `id_topic` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_topic_category_accepted`
--

CREATE TABLE `t_topic_category_accepted` (
  `id_category` int(11) NOT NULL,
  `id_topic_acc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(11) NOT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `date_joined` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `number_hp` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `verified` bit(1) DEFAULT NULL,
  `work` varchar(255) DEFAULT NULL,
  `work_addres` varchar(255) DEFAULT NULL,
  `work_city` varchar(255) DEFAULT NULL,
  `work_country` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `activation_code`, `address`, `city`, `country`, `date_joined`, `email`, `fullname`, `number_hp`, `password`, `picture`, `sex`, `summary`, `username`, `verified`, `work`, `work_addres`, `work_city`, `work_country`) VALUES
(1, '1', 'tes', 'te', 'tes', '15/05/1245', 'tes', 'tes', 222, 'tes', 'tes', 'te', 'tes', 'tes', b'1111111111111111111111111111111', 'tes', 'te', 'tes', 'tes'),
(2, '2', 'oke', 'okes', 'indo', NULL, 'vandreas19@yahoo.com', 'Vicky', 8122141, 'vandreas', 'tesd', 'Male', 'Oke', 'vicky', b'1111111111111111111111111111111', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_category`
--

CREATE TABLE `t_user_category` (
  `id_category` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_category`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `t_comment`
--
ALTER TABLE `t_comment`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `FKq0pfhlt4elq74bht5s74283yi` (`id_topic_acc`),
  ADD KEY `FKswomukjc2h74ouvkvavu75cmb` (`id_notification`),
  ADD KEY `FKmnlt4omh137d99e33xjmspb0e` (`id_topic`);

--
-- Indexes for table `t_notification`
--
ALTER TABLE `t_notification`
  ADD PRIMARY KEY (`id_notification`),
  ADD KEY `FKgpecufg46j9i7pfgoy9ycujyn` (`id_comment`),
  ADD KEY `FKlocwjcv4vyef74hsmcq42txmt` (`id_user`);

--
-- Indexes for table `t_topic`
--
ALTER TABLE `t_topic`
  ADD PRIMARY KEY (`id_topic`),
  ADD KEY `FKns7jxiagjhwsfwii02j0w5col` (`id_user`);

--
-- Indexes for table `t_topic_accepted`
--
ALTER TABLE `t_topic_accepted`
  ADD PRIMARY KEY (`id_topic_acc`),
  ADD KEY `FKhtf3m1bfx7dqgd4w570uoksrb` (`id_user`);

--
-- Indexes for table `t_topic_category`
--
ALTER TABLE `t_topic_category`
  ADD PRIMARY KEY (`id_topic`,`id_category`),
  ADD UNIQUE KEY `UK_6c9wg2xlyxwqg3jr07ta6s42` (`id_topic`),
  ADD KEY `FKbm1ltkrv433buxljxyyid72bw` (`id_category`);

--
-- Indexes for table `t_topic_category_accepted`
--
ALTER TABLE `t_topic_category_accepted`
  ADD PRIMARY KEY (`id_category`,`id_topic_acc`),
  ADD UNIQUE KEY `UK_lx8dy3lrsq1kv648ifyx6qslj` (`id_topic_acc`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `t_user_category`
--
ALTER TABLE `t_user_category`
  ADD PRIMARY KEY (`id_user`,`id_category`),
  ADD UNIQUE KEY `UK_7gxspq4sylc1h0o4yryp0a3lj` (`id_category`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_category`
--
ALTER TABLE `t_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_comment`
--
ALTER TABLE `t_comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_notification`
--
ALTER TABLE `t_notification`
  MODIFY `id_notification` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_topic`
--
ALTER TABLE `t_topic`
  MODIFY `id_topic` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_topic_accepted`
--
ALTER TABLE `t_topic_accepted`
  MODIFY `id_topic_acc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
