package proyek.psi.itc.model;

import java.util.Date;
import java.util.Set;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "t_topic")
public class Topic {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_topic")
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user")
	private User user;
	
	public Topic() {}
	
	public Topic(int id, Boolean status, int totalagree, String title, String description, String picture) {
		super();
		this.id = id;
		this.status = status;
		this.totalagree = totalagree;
		this.title = title;
		this.description = description;
		this.picture = picture;
	}
	
	@OneToMany(mappedBy = "topic")
	private Set<Comment>comment;
	
	@OneToMany(mappedBy = "topic")
	private Set<TopicCategory> topiccategory;
	
	
	/*@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "t_topic_category", joinColumns = @JoinColumn(name = "id_topic"), inverseJoinColumns = @JoinColumn(name = "id_category"))
	private Set<Category>category;
	*/
	@Column(name = "status")
	private boolean status;
	
	@Column(name = "total_agree", columnDefinition = "integer DEFAULT 0")
	private int totalagree;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "picture")
	private String picture;
	
	@Column(name = "date_created")
	private String datecreated;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public Set<Comment> getComment() {
		return comment;
	}

	public void setComment(Set<Comment> comment) {
		this.comment = comment;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getTotalagree() {
		return totalagree;
	}

	public void setTotalagree(int totalagree) {
		this.totalagree = totalagree;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getDatecreated() {
		return datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Set<TopicCategory> getTopiccategory() {
		return topiccategory;
	}

	public void setTopiccategory(Set<TopicCategory> topiccategory) {
		this.topiccategory = topiccategory;
	}

	/*public Set<Category> getCategory() {
		return category;
	}

	public void setCategory(Set<Category> category) {
		this.category = category;
	}*/
	
	
	
	
}

