package proyek.psi.itc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name = "t_topic_category")
public class TopicCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_topic_category;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_topic")
	private Topic topic;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_category")
	private Category category;

	public int getId_topic_category() {
		return id_topic_category;
	}

	public void setId_topic_category(int id_topic_category) {
		this.id_topic_category = id_topic_category;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
	
	
	
}
