package proyek.psi.itc.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.json.JSONObject;
import org.json.JSONArray;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
@Table(name = "t_category")
public class Category {
	
	@Id
	@Column(name="id_category")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_category;
	
	@Column(name="name")
	private String name;
	
	@OneToMany(mappedBy = "category")
	private Set<TopicCategory> topiccategory;
	

/*	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "t_user_category", joinColumns = @JoinColumn(name = "id_category"), inverseJoinColumns = @JoinColumn(name = "id_user"))
	private Set<User> users;*/
	
	public Integer getId_category() {
		return id_category;
	}

	public void setId_category(Integer id_category) {
		this.id_category = id_category;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
/*	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}*/
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "t_topic_category", joinColumns = @JoinColumn(name = "id_category"), inverseJoinColumns = @JoinColumn(name = "id_topic"))
	private Set<Topic>topic;
	
	public Set<Topic> getTopic() {
		return topic;
	}

	public void setTopic(Set<Topic> topic) {
		this.topic = topic;
	}

	public Set<TopicCategory> getTopiccategory() {
		return topiccategory;
	}

	public void setTopiccategory(Set<TopicCategory> topiccategory) {
		this.topiccategory = topiccategory;
	}
	
	

}
