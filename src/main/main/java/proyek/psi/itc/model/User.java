package proyek.psi.itc.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "t_user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_user;

	@OneToMany(mappedBy = "user")
	private Set<Topic>topic;
	
	@OneToMany(mappedBy = "user")
	private Set<Comment>comment;

	
	@OneToMany(mappedBy = "user")
	private Set<Notification>notification;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "t_user_category", joinColumns = @JoinColumn(name = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_category"))
	private Set<Category>category;

	@Length(min = 5, message = "*Your password must have at least 5 characters")
	@Column(name = "password")
	private String password;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "fullname")
	private String fullname;
	
	@Column(name = "number_hp")
	private String numberhp;
	
	@Column(name = "verified")
	private boolean verified;
	
	@Column(name = "summary")
	private String summary;
	
	@Column(name = "date_joined")
	private String datejoined ;
	
	@Column(name = "activation_code")
	private String activationcode;
	
	@Column(name = "sex")
	private String sex;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "work")
	private String work;
	
	@Column(name = "work_addres")
	private String work_addres;
	
	@Column(name = "work_city")
	private String work_city;
	
	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public String getWork_addres() {
		return work_addres;
	}

	public void setWork_addres(String work_addres) {
		this.work_addres = work_addres;
	}

	public String getWork_city() {
		return work_city;
	}

	public void setWork_city(String work_city) {
		this.work_city = work_city;
	}

	public String getWork_country() {
		return work_country;
	}

	public void setWork_country(String work_country) {
		this.work_country = work_country;
	}

	@Column(name = "work_country")
	private String work_country;
	
	@Column(name = "picture")
	private String picture;


	
	/* in konstruktornya */ 
	public User (String username, String password, String fullname, String sex, String numberhp, String city, String country, String address) {
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.numberhp = numberhp;
		this.city = city;
		this.country = country;
		this.address = address;
		this.sex = sex;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public Set<Topic> getTopic() {
		return topic;
	}

	public void setTopic(Set<Topic> topic) {
		this.topic = topic;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getNumberhp() {
		return numberhp;
	}

	public void setNumberhp(String numberhp) {
		this.numberhp = numberhp;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDatejoined() {
		return datejoined;
	}

	public void setDatejoined(String DateJoined) {
		this.datejoined = DateJoined;
	}

	public String getActivationcode() {
		return activationcode;
	}

	public void setActivationcode(String activationcode) {
		this.activationcode = activationcode;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Set<Category> getCategory() {
		return category;
	}

	public void setCategory(Set<Category> category) {
		this.category = category;
	}

	public Set<Notification> getNotification() {
		return notification;
	}

	public void setNotification(Set<Notification> notification) {
		this.notification = notification;
	}

	public Object getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Comment> getComment() {
		return comment;
	}

	public void setComment(Set<Comment> comment) {
		this.comment = comment;
	}
	
	
	
	
	
	
}
