package proyek.psi.itc.Repository;

import org.springframework.data.repository.CrudRepository;

import proyek.psi.itc.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}
