package proyek.psi.itc.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import proyek.psi.itc.model.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Integer>{
	List<Topic> findByTotalagree(int totalagree);
	Topic findAllById(Integer id_topic);
	
}
