package proyek.psi.itc.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import proyek.psi.itc.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer>{

}
