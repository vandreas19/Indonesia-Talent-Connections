package proyek.psi.itc.Repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import proyek.psi.itc.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer>{
}
