package proyek.psi.itc.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import proyek.psi.itc.model.Category;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.TopicCategory;

public interface TopicCategoryRepository extends JpaRepository<TopicCategory, Integer>{
	List<TopicCategory> findAllByTopic(List<Topic> topic);
	
}
