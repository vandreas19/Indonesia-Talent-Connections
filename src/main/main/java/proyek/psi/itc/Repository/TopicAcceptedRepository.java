package proyek.psi.itc.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.TopicAccepted;

public interface TopicAcceptedRepository extends JpaRepository<TopicAccepted, Integer> {

	void save(Topic s);

}
