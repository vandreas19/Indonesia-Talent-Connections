package proyek.psi.itc.controller;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import proyek.psi.itc.Repository.CommentRepository;
import proyek.psi.itc.Repository.TopicCategoryRepository;
import proyek.psi.itc.dao.CommentDao;
import proyek.psi.itc.dao.TopicDao;
import proyek.psi.itc.model.Comment;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.TopicCategory;
import proyek.psi.itc.model.User;

@Controller
public class CommentController {
	@Autowired
	private CommentDao commentDao;
	@Autowired
	private CommentRepository cRepo;
	@Autowired
	private TopicDao topicDao;
	DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date dates = new Date();
	@Autowired
	private TopicCategoryRepository tcRepo;
	@Autowired
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	@RequestMapping(value="/issuedetail/{id}" ,method = RequestMethod.GET)
	public String tampilkanIssueDetail(Model model, Comment comment, @PathVariable Integer id, HttpServletRequest req) {
		Topic top = topicDao.getIdTopic(id);
		User users = (User) req.getSession().getAttribute("user");
		List<Comment> comments = commentDao.getDataByIdTopic(id);
		
		for(int i=0;i<comments.size();i++) {
			model.addAttribute("trues", false);
			model.addAttribute("truess", false);
			model.addAttribute("all", false);
			model.addAttribute("half", false);
			model.addAttribute("halfs", false);
			if(comments.get(i).isStatusverification() == true) {
				model.addAttribute("trues", true);
				System.out.println("TOL");
			}else {
				model.addAttribute("truess", true);
				if(top.getUser().getId_user() == users.getId_user()) {
					model.addAttribute("all", true);
					model.addAttribute("half", true);
				}
				
				for(int j=0;j<comments.size();j++) {
					if(comments.get(j).getUser().getId_user() == users.getId_user() && users.getId_user() != top.getUser().getId_user()) {
						model.addAttribute("halfs", true);
					}
				}
			}

			
		}
		
	
	//	TopicCategory tcat = tcRepo.findByTopic(top);
		model.addAttribute("topic", top);
	//	model.addAttribute("f", tcat);
		model.addAttribute("comm", comments);
		return "issuedetail";
	}
	
	
	
	
	
	
		
	@RequestMapping(value="/addcommend/{id}" ,method = RequestMethod.POST)
	public String tampilkanComment(Model model, @Valid Comment comment, @PathVariable Integer id, HttpServletRequest request) {
		Topic top = topicDao.getIdTopic(id);
		User users = (User) request.getSession().getAttribute("user");
//		model.addAttribute("topic", top);
		comment.setTopic(top);
		comment.setUser(users);
		comment.setDatecreated(date.format(dates));
		comment.setStatusverification(false);
		commentDao.saveOrUpdate(comment);
		model.addAttribute("comment", new Comment());
		return "redirect:/issuedetail/{id}";
	}
	
	@RequestMapping(value="/issuedetail/verified/{id}", method = RequestMethod.POST)
	public String verifiedComment(Model model, @PathVariable Integer id, @Valid Comment comment) {
		Comment com = commentDao.getIdComment(id);
		com.setStatusverification(true);
		commentDao.saveOrUpdate(com);
		model.addAttribute("commend", com);
		return "redirect:/issudetail/{id}";
	}
	
}
