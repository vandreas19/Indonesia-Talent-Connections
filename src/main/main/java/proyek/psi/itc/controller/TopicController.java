package proyek.psi.itc.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import proyek.psi.itc.model.Category;
import proyek.psi.itc.model.Comment;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.TopicCategory;
import proyek.psi.itc.model.User;
import proyek.psi.itc.Repository.CategoryRepository;
import proyek.psi.itc.Repository.CommentRepository;
import proyek.psi.itc.Repository.TopicAcceptedRepository;
import proyek.psi.itc.Repository.TopicCategoryRepository;
import proyek.psi.itc.Repository.TopicRepository;
import proyek.psi.itc.Repository.UserRepository;
import proyek.psi.itc.dao.TopicDao;
import proyek.psi.itc.dao.UserDao;
import proyek.psi.itc.service.TopicService;
import proyek.psi.itc.service.UserService;


@Controller
public class TopicController {
	@Autowired
	private TopicDao topicDao;
	@Autowired
	private TopicRepository trepo;
	@Autowired
	private CommentRepository cRepo;
	@Autowired
	private TopicCategoryRepository tcRepo;
	
	DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date dates = new Date();
	@Autowired
	private UserDao ud;
	@Autowired
	private UserRepository ur;
	@Autowired
	private TopicService topicService;
	
	@Autowired
	private CategoryRepository crepo;
	@Autowired
	private CommentRepository corepo;
	
	@Autowired
	public void setTopicService(TopicService topicService) {
		this.topicService = topicService;
	}

	@Autowired
	private TopicAcceptedRepository taRepo;
	@Autowired
	public void setTopicDao(TopicDao topicDao) {
		this.topicDao = topicDao;
	}
	@Autowired
	private UserService userService;
	@Autowired
	private UserDao userDao;
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	//====================================================================== ke issue.html
	@RequestMapping("/issue")
	public String TopicList(Model model) {
		
		model.addAttribute("searchTitle");

		List<Topic> to = trepo.findAll();
		List<TopicCategory> lt = tcRepo.findAll();
		List<TopicCategory> ltc = new ArrayList<TopicCategory>();
		
		
		
		for(int i=0;i<to.size();i++) {
			if(to.get(i).getTotalagree() == 5) {
				for(int j=0;j<lt.size();j++) {
					if(lt.get(j).getTopic().getId() == to.get(i).getId()) {
						ltc.add(lt.get(i));
					}
				}
			}
		}
		model.addAttribute("topic",ltc);
		return "issue";
	}
	
	@RequestMapping(value = "/hapuscommend/{id}", method = RequestMethod.POST)
	public String hapuscomment(@PathVariable Integer id, Model model) {
		cRepo.deleteById(id);
		return "redirect:/issue";
	}
	
	@RequestMapping(value = "/verifcommend/{id}", method = RequestMethod.POST)
	public String verifcomment(@PathVariable Integer id, Model model) {
		Comment co = corepo.getOne(id);
		co.setStatusverification(true);
		corepo.save(co);
		return "redirect:/issue";
	}
	
	//====================================================================== ke isi.html
	@RequestMapping(value="/isi" ,method = RequestMethod.GET)
	public String tampilkanForm(Model model, Topic topic) {
		model.addAttribute("topic", new Topic());
		return "isi";
	}
	
	//====================================================================== ke vote.html
	@RequestMapping(value="/vote" ,method = RequestMethod.GET)
	public String tampilkanVote(Model model, Topic topic) {
		List<Topic> li = topicDao.listTopic();
		List<Topic> simpan = new ArrayList<Topic>();
		for(int i=0;i<li.size();i++) {
			if(li.get(i).getTotalagree() < 5) {
				simpan.add(li.get(i));
			}
		}
		model.addAttribute("topic", simpan);
		return "vote";
	}
	
	//====================================================================== nampil dari vote.html	
	@RequestMapping(value="/topic/create/{id}" ,method = RequestMethod.POST)
	public String agreeTopic(Model model, @Valid Topic topic,@PathVariable Integer id) {
		topicService.UpdateAgreeById(id);
		return "redirect:/vote";
	}
	
	//====================================================================== nampil dari vote.html	
	@RequestMapping(value="/topic/add/{id}" ,method = RequestMethod.POST)
	public String simpanTopic(Model model, @Valid Topic topic ,@PathVariable Integer id, HttpServletRequest request,  TopicCategory tc,
		@RequestParam("id_category") int id_category,Category category, @RequestParam("files") MultipartFile file) {
		User users = (User) request.getSession().getAttribute("user");
		
		//model.addAttribute("topic", topicDao.saveOrUpdate(topic));
		topic.setUser(users);
		if(!file.isEmpty()) {
			topic.setPicture(file.getOriginalFilename());
			//wisata.setGambar(file.getOriginalFilename());
			String name = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("E:/C/itcnew/src/main/resources/static/images/"+name)));
				stream.write(bytes);
				stream.flush();
				stream.close();
				System.out.println(stream);
				} catch (Exception e) {
				return "Failed to upload image" +e.getMessage();
			}
		}
		topic.setDatecreated(date.format(dates));
		trepo.save(topic);
		List<Category> l = new ArrayList<Category>();
		List<Topic> q = topicDao.listTopic();
	//	List<TopicCategory> tca = new ArrayList<TopicCategory>();
			tc.setCategory(category);
			tc.setTopic(q.get(q.size()-1));
			tcRepo.save(tc);
			//tca.clear();
		return "redirect:/vote";
	}
	
	//====================================================================== menampilkan tabel topic yang telah dibuat ke profil.html
	
	@RequestMapping("/profil/{id}")
	public String tampil(Model model, HttpServletRequest request, @PathVariable Integer id) {
		User users = (User) request.getSession().getAttribute("user");
		Integer sum = 0;
		if(users.getId_user() != id)id = users.getId_user();
		List<Topic> li = topicDao.getDataByIdUser(id);
		List<Topic> baru = new ArrayList<Topic>();
		for(int i=0;i<li.size();i++) {
			if(li.get(i).getTotalagree() == 5) {
				baru.add(li.get(i));
				sum = sum + 1;
			}
		}
		
		model.addAttribute("topic", baru);
		model.addAttribute("count", sum);
		model.addAttribute("user", userService.getUserByIdUser(id));
		return "profil";
	}
	
	

	//====================================================================== ?
	@RequestMapping(value="/topic/edit/{id}" ,method = RequestMethod.GET)
	public String editData(@PathVariable Integer id, Model model, HttpServletRequest request,  @Valid Topic topic) {
		model.addAttribute("topic", topicDao.getDataById(id));
		model.addAttribute("topics", topicDao.getIdTopic(id));
		return "isiedit";
	}
	
	@RequestMapping(value="/topic/edit/{id}" ,method = RequestMethod.POST)
	public String editDatsa(@PathVariable Integer id, Model model, HttpServletRequest request) {
		Topic top = topicDao.getIdTopic(id);
		User users = (User) request.getSession().getAttribute("user");
		String title = request.getParameter("title");
		String content = request.getParameter("description");
		top.setTitle(title);
		top.setDescription(content);
		top.setDatecreated(date.format(dates));
		top.setTotalagree(5);
		top.setUser(users);
		trepo.save(top);
		
		
		return "redirect:/profil/{id}";
	}
	
	//====================================================================== ?	
	@RequestMapping(value="/topic/hapus/{id}")
	public String hapus(@PathVariable Integer id) {
		topicService.hapus(id);
		return "redirect:/profil/{id}";
	}
	
//	//====================================================================== ?	
//	@RequestMapping(value="/editvote/{id}" ,method = RequestMethod.POST)
//	public String updatetotalagree(@PathVariable Integer id, Model model) {
//		Topic row = tRepo.findByIdTopic(id);
//		model.addAttribute("topic", tRepo.setTotalAgree(row.getTotalagree(), row.getId_topic()));
//		return "redirect:/vote";
//	}	
//	
	//====================================================================== ?
	
	@RequestMapping(value = "/editsum/{id}", method = RequestMethod.GET)
	public String editss(Model model, User user) {
		model.addAttribute("user", new User());
		return "summary";
	}
	
	
	//====================================================================== ?
	@RequestMapping(value = "/editsum/{id}", method = RequestMethod.POST)
	public String edits(@PathVariable Integer id, Model model, User user, HttpServletRequest request) {
		String summary = request.getParameter("summary");
		User users = userDao.getUserByIdUser(id);
		users.setSummary(summary);
		userDao.saveUser(users);
		return "redirect:/profil/{id}";
	}
	
	//====================================================================== ?
	@RequestMapping(value = "/editprofil/{id}" ,method = RequestMethod.GET)
	public String tampilkanPeople(@PathVariable Integer id, Model model) {
		model.addAttribute("user", ud.getUserByIdUser(id));
		return "profiledit";
	}
	
	//====================================================================== ?
	@RequestMapping(value = "/editprofil/{id}" ,method = RequestMethod.POST)
	public String tampilkanPeoples(@PathVariable Integer id, Model model, HttpServletRequest req, @RequestParam("files") MultipartFile file) {
		User users = ud.getUserByIdUser(id);
		String fullname = req.getParameter("fullname");
		String nohp = req.getParameter("numberhp");
		String city = req.getParameter("city");
		String address = req.getParameter("address");
		String country = req.getParameter("country");
		users.setFullname(fullname);
		users.setNumberhp(nohp);
		users.setCity(city);
		users.setAddress(address);
		users.setCountry(country);
		if(!file.isEmpty()) {
			users.setPicture(file.getOriginalFilename());
			//wisata.setGambar(file.getOriginalFilename());
			String name = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("E:/C/itcnew/src/main/resources/static/images/"+name)));
				stream.write(bytes);
				stream.flush();
				stream.close();
				System.out.println(stream);
				} catch (Exception e) {
				return "Failed to upload image" +e.getMessage();
			}
		}
		
		ur.save(users);
		return "redirect:/profil/{id}";
	}

	//====================================================================== ?
		@RequestMapping(value = "/search")
		public String searchTopic(@RequestParam("searchTitle") String title, Model model, User user, HttpServletRequest request) {
			if (title != null) {
				List<Topic> list = topicService.searchByTitle(title);
				System.out.println(""+list.get(1).getId());
				List<TopicCategory> lt = tcRepo.findAll();
				List<TopicCategory> ltc = new ArrayList<TopicCategory>();
				
				
				
				for(int i=0;i<list.size();i++) {
					if(list.get(i).getTotalagree() == 5) {
						for(int j=0;j<lt.size();j++) {
							if(lt.get(j).getTopic().getId() == list.get(i).getId()) {
								ltc.add(lt.get(i));
							}
						}
					}
				}
				model.addAttribute("topicsfound",ltc);
				ltc.clear();
				return "/issue";
			}else return "redirect:/issue";

		}
		
		
		
		
		
			
	
	
}
