package proyek.psi.itc.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import proyek.psi.itc.model.SessionCheck;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.User;
import proyek.psi.itc.Repository.UserRepository;
import proyek.psi.itc.dao.TopicDao;
import proyek.psi.itc.dao.UserDao;
import proyek.psi.itc.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserDao userDao;
	
	DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date dates = new Date();
	
	@Autowired
	private TopicDao topicDao;
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Autowired
	private UserService userService;
	private User user;
	
	@Autowired
	SessionCheck sessionCheck;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	private String savedUser;
	
	@RequestMapping("/index")
	public String loginPage(Model model) {
		List<Topic> n = topicDao.listTopic();
		List<Topic> news = new ArrayList<Topic>();
		for(int i=0;i<n.size();i++) {
			if(n.get(i).getTotalagree() == 5) {
				news.add(n.get(i));
			}
		}
		model.addAttribute("top", news);
		return "index";
	}
	
	//========================================================================================================================
	@RequestMapping("/")	
	public String tampilkanHome (Model model, HttpServletRequest request){
		model.addAttribute("user", new User());
		return "masuk";
	}
	
	//========================================================================================================================
	
	@RequestMapping(value="/masuk", method=RequestMethod.GET)
	public String formLogin(Model model, HttpServletRequest request){
		
		return "masuk";
	}
	
	@RequestMapping(value="/masuk/user", method=RequestMethod.GET)
	public String formsLogin(Model model, HttpServletRequest request){
		
		return "masuk";
	}
	
	
	
	//========================================================================================================================
	
	@RequestMapping(value = "/masuk/user", method = RequestMethod.POST)
	public String tampilkanLogin(Model model, HttpServletRequest request) {
		/*if(sessionCheck.running(request)) {
			model.addAttribute("ssession", "LOG OUT");
			model.addAttribute("sessionhref", "logout");
		}else {
			model.addAttribute("ssession", "LOGIN");
			model.addAttribute("sessionhref", "login");
		}
		
		String username = request.getParameter("user");
		String password = request.getParameter("pass");
		
		if (username.equals("admin") && password.equals("admin123")){
			request.getSession().setAttribute("admin",true);
			sessionCheck.sessionLogin(request, username);
			savedUser = username;
			return "redirect:/";
		}
		else if (userService.login(username, password) != null) {
			
			request.getSession().setAttribute("user", userService.login(username, password));
			user = (User) request.getSession().getAttribute("pelanggan");
			sessionCheck.sessionLogin(request, username);
			savedUser = username;
			return "redirect:/";
		}*/
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		User users = userService.login(user, pass);
		if(users == null) {
			return "redirect:/";
		}else {
			request.getSession().setAttribute("user", users);
			return "redirect:/index";
		}
	}
	//========================================================================================================================
	@RequestMapping(value="/logout")
	public String formLogout(Model model, HttpServletRequest request , HttpServletResponse response){
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if(auth != null) {
//			new SecurityContextLogoutHandler().logout(request, response, auth);
//		}
		List<Topic> n = topicDao.listTopic();
		List<Topic> news = new ArrayList<Topic>();
		for(int i=0;i<n.size();i++) {
			if(n.get(i).getTotalagree() == 5) {
				news.add(n.get(i));
			}
		}
		model.addAttribute("topic", news);
		return "indexguest";
	}
	
	@Autowired
	UserRepository userRep;
	
	@RequestMapping(value = "/daftar")
	public String formDf(Model model, User user) {
		model.addAttribute("user", new User());
		return "masuk";
	}
	
	@RequestMapping(value="/daftar", method = RequestMethod.POST)
	public String formDaftar(Model model, HttpServletRequest request , HttpServletResponse response, @Valid User user){
		user.setDatejoined(date.format(dates));
		userDao.saveUser(user);
		model.addAttribute("okey", true);
		return "redirect:/";
	}	
	
	@RequestMapping("/oke")
	public String u() {
		return "category";
	}
	
	
}
