package proyek.psi.itc.controller;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import proyek.psi.itc.Repository.UserRepository;
import proyek.psi.itc.dao.TopicDao;
import proyek.psi.itc.dao.UserDao;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.User;

import javax.validation.Valid;


@Controller
public class PageController {
	@Autowired
	private TopicDao topicDao;
	@Autowired
	private UserDao ud;
	// Untuk redirect ke halaman peopledetail.html
	@RequestMapping(value="/peopledetail/{id}" ,method = RequestMethod.GET)
	public String tampilkanPeopledetail(Model model, @PathVariable Integer id) {
		User use = ud.getUserByIdUser(id);
		model.addAttribute("users", use);
		return "peopledetail";
	}

	// Untuk redirect ke halaman people.html
	@RequestMapping(value = "/people" ,method = RequestMethod.GET)
	public String tampilkanPeople(Model model) {
		model.addAttribute("user", ud.listUser());
		return "people";
	}
	
	
	
	
	
}
