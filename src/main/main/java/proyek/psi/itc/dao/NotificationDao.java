package proyek.psi.itc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import proyek.psi.itc.model.Comment;
import proyek.psi.itc.model.Notification;
import proyek.psi.itc.service.NotificationService;

@Service
public class NotificationDao implements NotificationService {

	private EntityManagerFactory emf;
	
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	@Override
	public List<Notification> listNotification() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Notification", Notification.class).getResultList();
	}

	@Override
	public Notification saveOrUpdate(Notification notification) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Notification saved = em.merge(notification);
		em.getTransaction().commit();
		return saved;
	}

}