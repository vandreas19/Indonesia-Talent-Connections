package proyek.psi.itc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import proyek.psi.itc.service.CommentService;
import proyek.psi.itc.model.Comment;
import proyek.psi.itc.model.Topic;

@Service
public class CommentDao implements CommentService {

	private EntityManagerFactory emf;

	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public List<Comment> listComment() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Comment", Comment.class).getResultList();
	}

	@Override
	public Comment saveOrUpdate(Comment comment) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Comment saved = em.merge(comment);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public void hapus(Integer id_comment) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Topic.class, id_comment));
		em.getTransaction().commit();
	}

	@Override
	public List<Comment> getDataById(Integer id_comment) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Comment where id_comment ='" + id_comment + "'", Comment.class).getResultList();
	}

	@Override
	public List<Comment> getDataByIdTopic(Integer id_topic) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Comment where id_topic ='" + id_topic + "'", Comment.class).getResultList();
	}

	@Override
	public Comment getIdComment(Integer id_comment) {
		EntityManager em = emf.createEntityManager();
		return em.find(Comment.class, id_comment);
	}

}
