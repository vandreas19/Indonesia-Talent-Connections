package proyek.psi.itc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import proyek.psi.itc.service.TopicService;
import proyek.psi.itc.model.Topic;
import proyek.psi.itc.model.User;

@Service
@EnableTransactionManagement
public class TopicDao implements TopicService {

	private EntityManagerFactory emf;

	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public List<Topic> listTopic() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Topic", Topic.class).getResultList();
	}

	@Override
	public Topic saveOrUpdate(Topic topic) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Topic saved = em.merge(topic);
		em.getTransaction().commit();
		em.close();
		return saved;
	}

	@Override
	public Topic getIdTopic(Integer id) {
		EntityManager em = emf.createEntityManager();
		return em.find(Topic.class, id);
	}

	@Override
	public List<Topic> getDataById(Integer id_topic) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Topic where id_topic ='" + id_topic + "'", Topic.class).getResultList();
	}

	@Override
	public List<Topic> getDataByIdUser(Integer id_user) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Topic where id_user ='" + id_user + "'", Topic.class).getResultList();
	}

	@Override
	public List<Topic> getTotalAgree(Integer totalagree) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Topic where totalagree ='" + totalagree + "'", Topic.class).getResultList();
	}

	@Override
	public Topic saveOrUpdateAgree(Topic topic) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Topic saved = em.merge(topic);
		em.getTransaction().commit();
		em.close();
		return saved;

	}

	@Override
	@Transactional
	public void UpdateAgreeById(Integer id) {
		Topic topic = getIdTopic(id);
		topic.setTotalagree(topic.getTotalagree() + 1);
		topic = saveOrUpdate(topic);

	}

	@Override
	public void hapus(Integer id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(Topic.class, id));
		em.getTransaction().commit();
	}

	@Override
	public List<Topic> searchByTitle(String title) {
		EntityManager em = emf.createEntityManager();
		TypedQuery<Topic> query = em.createQuery("select t from Topic t where t.title LIKE :searchKeyword", Topic.class);
		query.setParameter("searchKeyword","%"+title+"%");
		
		return query.getResultList();
	}

}
