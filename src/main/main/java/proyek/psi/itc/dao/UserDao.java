package proyek.psi.itc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import proyek.psi.itc.service.UserService;
import proyek.psi.itc.model.User;

@Service
public class UserDao implements UserService {

	private EntityManagerFactory emf;
	
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}


	@Override
	public List<User> listUser() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from User", User.class).getResultList();	
	}
	
	@Override
	public User getUserByIdUser(int id_user) {
		EntityManager em = emf.createEntityManager();
		return em.find(User.class, id_user);
	}
	
	@Override
	public User saveUser(User user) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		User saved = em.merge(user);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public void deleteUser(int id_user) {
		EntityManager em= emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(User.class, id_user));
		em.getTransaction().commit();	
	}
	
	@Override
	public User login(String username, String password) {
		List<User> liu = listUser();
		for(int i =0; i<liu.size(); i++) {
			if(liu.get(i).getUsername().equals(username) && (liu.get(i).getPassword().equals(password))) {
				return liu.get(i);
			}
		}
		return null;
	}	
}