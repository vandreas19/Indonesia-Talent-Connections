package proyek.psi.itc.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import proyek.psi.itc.model.User;

@Repository
public interface UserService {
	public User getUserByIdUser(int id_user);
	public User saveUser(User user);
	public void deleteUser(int id_user);
	List <User> listUser();
	public User login(String username, String password);
}
