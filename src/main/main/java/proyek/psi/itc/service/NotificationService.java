package proyek.psi.itc.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import proyek.psi.itc.model.Notification;

@Repository
public interface NotificationService {
	List <Notification> listNotification();
	Notification saveOrUpdate(Notification notification);
}
