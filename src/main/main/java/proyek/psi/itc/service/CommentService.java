package proyek.psi.itc.service;
import java.util.List;

import org.springframework.stereotype.Repository;

import proyek.psi.itc.model.Comment;
import proyek.psi.itc.model.Topic;

@Repository
public interface CommentService {
	List <Comment> listComment();
	Comment saveOrUpdate(Comment comment);
	void hapus(Integer id_comment);
	public List<Comment> getDataByIdTopic(Integer id_topic);
	public List<Comment> getDataById(Integer id_comment);
	Comment getIdComment(Integer id_comment);
}
