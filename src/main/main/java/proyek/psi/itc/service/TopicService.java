package proyek.psi.itc.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import proyek.psi.itc.model.Topic;

@Repository
public interface TopicService {
	List <Topic> listTopic();
	Topic saveOrUpdate(Topic topic);
	Topic saveOrUpdateAgree(Topic topic);
	Topic getIdTopic(Integer id_topic);
	List<Topic> getTotalAgree(Integer totalagree);
	List<Topic> getDataById(Integer id_topic);
	void UpdateAgreeById(Integer id);
	void hapus(Integer id);
	public List<Topic> getDataByIdUser(Integer id_user);
	List<Topic> searchByTitle(String title);
}
